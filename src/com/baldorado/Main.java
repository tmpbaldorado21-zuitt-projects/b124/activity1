package com.baldorado;


import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String firstName;
        String lastName;
        double engGrade;
        double mathGrade;
        double sciGrade;

        Scanner appScan = new Scanner(System.in);

        System.out.println("Enter your firstname:");
        firstName = appScan.nextLine();

        System.out.println("Enter your lastname:");
        lastName = appScan.nextLine();

        System.out.println("Enter your grade in English:");
        engGrade = appScan.nextDouble();

        System.out.println("Enter your grade in Math:");
        mathGrade = appScan.nextDouble();

        System.out.println("Enter your grade in Science:");
        sciGrade = appScan.nextDouble();

        System.out.println("My name is " + firstName  + lastName + "and my grade is " + (engGrade + mathGrade + sciGrade)/3);
    }
}
